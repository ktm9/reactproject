import { Route, createBrowserRouter, createRoutesFromElements } from "react-router-dom";
import AdminLayout from "../layout/AdminLayout";
import AdminDashBoard from "../component/admin/DashBoard";
import UserLayout from "../layout/UserLayout";
import UserDashBoard from "../component/user/DashBoard";


export const Routers = createBrowserRouter(
    createRoutesFromElements(
      <Route>
         <Route path="/" element={<UserLayout/>}>
            <Route index element={<UserDashBoard/>}/>
            <Route path="admin" element={<AdminLayout/>}>
        </Route>
        
            {/* <Route index element={<AdminDashBoard/>}/> */}
        </Route>
       
        
       
      </Route>
    )
  );
  