import React from 'react'
import { NavLink, Outlet } from 'react-router-dom'

const DashBoard = () => {
  const link=[{
    name:"Admin",
    link:"/admin"
  },
  {
    name:"userAdmin",
    link:"useradmin"
  }
]
  return (
    <div>{
      link.map((item)=>{
        return(
          <NavLink
          to={item.link}
          style={({ isActive}) => {
            return {
              margin:"10px",
              color: isActive ? "primary" : "danger"
             

           
              
            };
          }}
          
          >
            {item.name}
          </NavLink>
        )
      })
      }
       
    </div>
   
  )
}

export default DashBoard