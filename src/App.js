import { Button, Checkbox, Form, Input, Select } from "antd";

const formSubmit = () => {
  const onFinish = (values) => {
    console.log("values", values);
  };
  const handleChange = (value) => {
    console.log(`selected ${value}`);
  };
  return (
    <div>
      <Form onFinish={onFinish}>
        <Form.Item
          name={"first_name"}
          label="First Name"
          rules={[{ required: true, message: "Please enter first name !" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name={"last_name"}
          label="Last Name"
          rules={[{ required: true, message: "Please enter last name !" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item name={"activate"}  valuePropName="checked">
          <Checkbox >active</Checkbox>
        </Form.Item>

        <Form.Item name={"textarea"} label="description">
          <Input.TextArea />
        </Form.Item>

        <Form.Item name={"password"} label="password">
          <Input.Password />
        </Form.Item>

        <Form.Item
          name={"email"}
          label="email"
          rules={[
            {
              type: "email",
              required: true,
              message: "Please enter valid email",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
      label="Select"
      name="Select"
      rules={[
        {
          required: true,
          message: 'Please input!',
        },
      ]}
    >
      {/* <Select /> */}
      <Select
      // defaultValue="lucy"
      style={{
        width: 120,
      }}
      onChange={handleChange}
      options={[
        {
          value: 'jack',
          label: 'Jack',
        },
        {
          value: 'lucy',
          label: 'Lucy',
        },
        {
          value: 'Yiminghe',
          label: 'yiminghe',
        },
        {
          value: 'disabled',
          label: 'Disabled',
          disabled: true,
        },
      ]}
    />
    </Form.Item>

        <Button htmlType="submit">Form Submit</Button>
      </Form>
    </div>
  );
};
export default formSubmit;

//label lekheko chai browser ma label dekhauna
//form .item chai form ko  types jastai text area,password haru rakhna
//rules rakheko chai validation ko lagi
//
