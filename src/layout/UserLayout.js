import React from 'react'
import { Outlet } from 'react-router-dom'

function UserLayout() {
  const link=[{
    name:"Admin",
    link:"/admin"
  },
  {
    name:"userAdmin",
    link:"useradmin"
  }
]
  return (
    <div>
     
      <div>
        <Outlet/>
    
      </div>
    </div>
  )
}

export default UserLayout
